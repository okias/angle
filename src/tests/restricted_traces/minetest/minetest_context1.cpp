#include "minetest_context1.h"
#include "trace_fixture.h"
#include "angle_trace_gl.h"

// Private Functions

void InitReplay()
{
    uint32_t kMaxBuffer = 218;
    uint32_t kMaxFenceNV = 0;
    uint32_t kMaxFramebuffer = 0;
    uint32_t kMaxMemoryObject = 0;
    uint32_t kMaxProgramPipeline = 0;
    uint32_t kMaxQuery = 0;
    uint32_t kMaxRenderbuffer = 0;
    uint32_t kMaxSampler = 0;
    uint32_t kMaxSemaphore = 0;
    uint32_t kMaxShaderProgram = 0;
    uint32_t kMaxTexture = 592;
    uint32_t kMaxTransformFeedback = 0;
    uint32_t kMaxVertexArray = 0;
    InitializeReplay("minetest.angledata.gz", 19704, 276, kMaxBuffer, kMaxFenceNV, kMaxFramebuffer, kMaxMemoryObject, kMaxProgramPipeline, kMaxQuery, kMaxRenderbuffer, kMaxSampler, kMaxSemaphore, kMaxShaderProgram, kMaxTexture, kMaxTransformFeedback, kMaxVertexArray);
}

// Public Functions

extern "C"
{
void ReplayFrame(uint32_t frameIndex)
{
    switch (frameIndex)
    {
        case 1:
            ReplayContext1Frame1();
            break;
        case 2:
            ReplayContext1Frame2();
            break;
        case 3:
            ReplayContext1Frame3();
            break;
        case 4:
            ReplayContext1Frame4();
            break;
        case 5:
            ReplayContext1Frame5();
            break;
        case 6:
            ReplayContext1Frame6();
            break;
        case 7:
            ReplayContext1Frame7();
            break;
        case 8:
            ReplayContext1Frame8();
            break;
        case 9:
            ReplayContext1Frame9();
            break;
        case 10:
            ReplayContext1Frame10();
            break;
        default:
            break;
    }
}

}  // extern "C"
